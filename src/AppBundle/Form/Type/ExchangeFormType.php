<?php

namespace AppBundle\Form\Type;


use AppBundle\Model\Exchange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExchangeFormType extends AbstractType
{
    private const PREFERRED_CURRENCY_LIST = [
        'EUR', 'USD', 'GBP'
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('baseCurrency', CurrencyType::class, [
                'preferred_choices' => self::PREFERRED_CURRENCY_LIST
            ])
            ->add('baseAmount', MoneyType::class, ['currency' => false])
            ->add('targetCurrency', CurrencyType::class, [
                'preferred_choices' => self::PREFERRED_CURRENCY_LIST
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Exchange::class
            ])
        ;

    }

    public function getName()
    {
        return 'app_exchange_form_type';
    }

}
