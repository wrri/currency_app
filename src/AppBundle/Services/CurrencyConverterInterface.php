<?php


namespace AppBundle\Services;


interface CurrencyConverterInterface
{
    public function convert(float $amount, string $fromCurrencyCode, string $toCurrencyCode): ?float;
}
