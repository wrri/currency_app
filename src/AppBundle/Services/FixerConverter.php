<?php

namespace AppBundle\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;

class FixerConverter implements CurrencyConverterInterface
{
    private const CONVERT_API_ENDPOINT = 'http://data.fixer.io/api/convert';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Client $client, string $apiKey, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
        $this->logger = $logger;
    }

    public function convert(float $amount, string $fromCurrencyCode, string $toCurrencyCode): ?float
    {

        try {
            $response = $this->client->request('GET', self::CONVERT_API_ENDPOINT, [
                'query' => [
                    'access_key' => $this->apiKey,
                    'from' => $fromCurrencyCode,
                    'to' => $toCurrencyCode,
                    'amount' => $amount
                ]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);
            return $response['result'] ?? null;
        } catch (ClientException $exception) {
            $this->logger->critical(sprintf("Error while converting %s from %s to %s", $amount, $fromCurrencyCode, $toCurrencyCode ));
        }

        return null;
    }
}
