<?php

namespace AppBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;


class Exchange
{

    /**
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $baseCurrency;

    /**
     * @Assert\NotBlank()
     *
     * @var float
     */
    private $baseAmount;

    /**
     * @Assert\NotBlank()
     *
     * @Assert\Expression(
     *     "this.getBaseCurrency() !== this.getTargetCurrency()",
     *     message="Please choose a different target currency to convert your amount!"
     * )
     *
     *
     * @var string
     */
    private $targetCurrency;


    public function getBaseCurrency(): ?string
    {
        return $this->baseCurrency;
    }

    public function setBaseCurrency(string $baseCurrency): self
    {
        $this->baseCurrency = $baseCurrency;

        return $this;
    }

    public function getBaseAmount(): ?float
    {
        return $this->baseAmount;
    }


    public function setBaseAmount(float $baseAmount): self
    {
        $this->baseAmount = $baseAmount;

        return $this;
    }

    public function getTargetCurrency(): ?string
    {
        return $this->targetCurrency;
    }

    public function setTargetCurrency(string $targetCurrency): self
    {
        $this->targetCurrency = $targetCurrency;

        return $this;
    }
}
