<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\ExchangeFormType;
use AppBundle\Model\Exchange;
use AppBundle\Services\CurrencyConverterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AppController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, CurrencyConverterInterface $converter)
    {
        $exchange = new Exchange();

        $form = $this->createForm(ExchangeFormType::class, $exchange);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getExchangeRate($exchange, $converter);
        }

        return $this->render('app/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
        ]);
    }

    private function getExchangeRate(Exchange $exchange, CurrencyConverterInterface $converter)
    {
        $exchangeResult = $converter->convert(
            $exchange->getBaseAmount(),
            $exchange->getBaseCurrency(),
            $exchange->getTargetCurrency()
        );

        if ($exchangeResult) {
            $this->addFlash('success', sprintf('Your result is %s', $exchangeResult));
        } else {
            $this->addFlash('error', 'Unexpected error. Please try again later.');
        }
    }
}
