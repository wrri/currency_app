<?php

namespace AppBundle\Test\Services;


use AppBundle\Services\FixerConverter;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Monolog\Logger;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\DependencyInjection\Tests\ContainerTest;

class FixerConverterTest extends TestCase
{

    public function testConvert()
    {

        $streamInterface = $this->getMockBuilder(StreamInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $streamInterface->method('getContents')
            ->willReturn('{"success":true,"query":{"from":"GBP","to":"USD","amount":12},"info":{"timestamp":1530245048,"rate":1.311406},"date":"2018-06-29","result":15.736872}')
        ;
        $messageInterface = $this->getMockBuilder(MessageInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
        $messageInterface->method('getBody')->willReturn($streamInterface);
        $client = $this->createMock(Client::class);
        $client->expects($this->once())
            ->method('request')
            ->willReturn($messageInterface)
        ;

        $logger = $this->createMock(Logger::class);

        $converter = new FixerConverter($client, "foo_bar", $logger);
        $result = $converter->convert(12, 'GBP', 'USD');

        $this->assertNotNull($result);
        $this->assertEquals(15.736872, $result);
    }
}
