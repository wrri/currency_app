currency_app
============

A Symfony app to convert currency using an API.

How to install
==============

1. clone this repo
2. run `composer install` and make sure to have a correct API key (needs at least basic plan, because of the endpoint) from fixer.io
3. run `php bin/console server:start`, it will start a server on that URL

How to run tests
==============
run `./vendor/bin/phpunit src/AppBundle/Tests/`
